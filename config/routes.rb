Rails.application.routes.draw do
  root "users#new"
  get  '/new',      to: 'static_pages#new'
  get  '/about',    to: 'static_pages#about'
  get  '/contact',  to: 'static_pages#contact'
  get    '/login',  to: 'sessions#new'
  post   '/login',  to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get  "/search",   to: "microposts#index"
  get  "/detail",   to: "microposts#detail"
  get '/auth/:provider/callback',    to: 'users#facebook_login',      as: :auth_callback
  get '/auth/failure',               to: 'users#auth_failure',        as: :auth_failure
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :microposts, only: [:create, :destroy] do
    resources :comments
  end
  resources :relationships,       only: [:create, :destroy]
  resources :likes,               only: [:create, :destroy]
end
