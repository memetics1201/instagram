[インスタクローン](https://vast-peak-39138.herokuapp.com)

## ●自分が苦労した点
###1.ルーティングの理解
  resourcesを利用して生成したURIをうまく活用することが難しかった。
  また、plefixを使ったパスの設定にも苦労した。
###2.facebookログイン
  今回はDeviceを使用せずに実装したため、特に試行錯誤が多かった。
###3.テストコード
  何をどうテストすれば良いのかわからず、あまり多くのテストコードを書くことができなかった。  

## ●学んだ点
###1. gemの仕様を理解する大切さ
  まずはomniauthがどのようなものなのか理解しないと実装できないことがわかった。
###2. MVCの基礎
　　　　モデルとコントローラーそれぞれに、どのような責務を負わせるか少し理解できた。
　　　　また、メソッドやモジュールの切り分け方も理解が深まった。
	
## ●自慢したい・相談したい点
ローカル環境と本番環境で動作が変わってしまう点。
ローカル環境では正しく遷移できたページが、本番環境ではエラーになってしまう箇所がある。

