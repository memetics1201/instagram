User.create!(fullname:  "Example User",
             username:  "example1234",
             email: "example@railstutorial.org",
             password:              "foobarbaz",
             password_confirmation: "foobarbaz",
             admin: true)

99.times do |n|
  fullname  = Faker::Name.name
  username  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(fullname:  fullname,
               username:  username,
               email: email,
               password:              password,
               password_confirmation: password)
end

# ポスト
users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end

# リレーションシップ
users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }
