require 'test_helper'
class UsersSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup information" do
    get root_path
    assert_no_difference 'User.count' do
      post users_path, params: { user: { username:  "",
                                         fullname: "kosuke abe",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    end
    assert_template 'users/new'
  end

  test "valid signup information" do
    get root_path
    assert_difference 'User.count', 1 do
      post users_path, params: { user: { username:  "kosuke1201",
                                         fullname: "kosuke abe",
                                         email: "user@example.com",
                                         password:              "password",
                                         password_confirmation: "password" } }
    end
    follow_redirect!
    assert_template 'users/show'
    assert_not flash.empty?
    assert is_logged_in?
  end
end
