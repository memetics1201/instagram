class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  # 新しい投稿を作成
  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "posted!"
      redirect_to new_url
    else
      @feed_items = []
      render 'static_pages/new'
    end
  end

  # 投稿を削除
  def destroy
    @micropost.destroy
    flash[:success] = "deleted"
    redirect_to new_url
  end

  # 投稿を検索
  def index
    @microposts = Micropost.search(params[:search])
  end

  # 投稿の詳細を表示
  def detail
    @micropost = Micropost.showdetail(params[:id])
    @comment = Comment.new
    @comments = Comment.where(micropost_id: params[:id])
  end

  private

  def micropost_params
    params.require(:micropost).permit(:content, :picture)
  end

  # 正しいユーザーでなければ初期ページへ遷移
  def correct_user
    @micropost = current_user.microposts.find_by(id: params[:id])
    redirect_to root_url if @micropost.nil?
  end
end
