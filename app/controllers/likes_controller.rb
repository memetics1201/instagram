class LikesController < ApplicationController
  # 投稿にいいねをする
  def create
    @micropost = Micropost.find(params[:micropost_id])
    unless @micropost.iine?(current_user)
      @micropost.iine(current_user)
      @micropost.reload
      respond_to do |format|
        format.html { redirect_to request.referrer || root_url }
        format.js { render inline: "location.reload();" }
      end
    end
  end

  # 投稿のいいねを削除する
  def destroy
    @micropost = Like.find(params[:id]).micropost
    if @micropost.iine?(current_user)
      @micropost.uniine(current_user)
      @micropost.reload
      respond_to do |format|
        format.html { redirect_to request.referrer || root_url }
        format.js { render inline: "location.reload();" }
      end
    end
  end
end
