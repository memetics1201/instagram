class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy, :following, :followers]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy

  # ユーザーの一覧を取得
  def index
    @users = User.paginate(page: params[:page])
  end

  # ユーザー固有のポストを取得
  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  # 新規ユーザー作成ページを取得
  def new
    @user = User.new
  end

  # 新規ユーザーを作成
  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = "Welcome to Instagram!"
      redirect_to @user
    else
      render 'new'
    end
  end

  # facebookログイン
  def facebook_login
    @user  = User.from_omniauth(request.env["omniauth.auth"])
    result = @user.save(context: :facebook_login)
    if result
      log_in @user
      flash[:success] = "FACEBOOKでログインしました。"
      redirect_to @user
    else
      redirect_to auth_failure_path
    end
  end

  # 認証に失敗
  def auth_failure
    @user = User.new
    flash[:danger] = "ログインに失敗しました"
    render 'new'
  end

  # ユーザー情報編集ページを取得
  def edit
    @user = User.find(params[:id])
  end

  # ユーザー情報を更新
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  # ユーザー情報を削除
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  # フォローしているユーザーを表示
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  # フォローされているユーザーを表示
  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

  def user_params
    params.require(:user).permit(:username, :fullname, :email, :password,
                                 :password_confirmation)
  end

  # 正しいユーザーかどうか確認
  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless @user == current_user
  end

  # 管理者かどうか確認
  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end
