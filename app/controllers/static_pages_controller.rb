class StaticPagesController < ApplicationController
  # 投稿フォームを表示、投稿一覧を表示
  def new
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def about
  end

  def contact
  end
end
